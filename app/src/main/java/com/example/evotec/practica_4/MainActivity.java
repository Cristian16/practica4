package com.example.evotec.practica_4;

import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, FragUno.OnFragmentInteractionListener, FragDos.OnFragmentInteractionListener{

    Button buttonFragUno, buttonFragDos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonFragUno = (Button) findViewById(R.id.btnFrgUno);
        buttonFragDos= (Button) findViewById(R.id.btnFrgDos);
        buttonFragUno.setOnClickListener(this);
        buttonFragDos.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnFrgUno:
                FragUno fragmentoUno = new FragUno();
                FragmentTransaction transsctionUno =getSupportFragmentManager().beginTransaction();
                transsctionUno.replace(R.id.contenedor,fragmentoUno);
                transsctionUno.commit();
                break;
            case R.id.btnFrgDos:
                FragDos fragmentoDos = new FragDos();
                FragmentTransaction transsctionDos =getSupportFragmentManager().beginTransaction();
                transsctionDos.replace(R.id.contenedor,fragmentoDos);
                transsctionDos.commit();
                break;

        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
